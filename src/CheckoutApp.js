import React, { useReducer } from "react";
import { Header } from "./components/Header";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { CheckoutContext } from "./context/checkout.context";
import { CheckoutReducer, initialState } from "./reducers/checkout.reducer";
import { Denied } from "./pages/Denied";
import { Checkout } from "./pages/Checkout";

export const CheckoutApp = () => {
  const [state, dispatch] = useReducer(CheckoutReducer, initialState);

  return (
    <CheckoutContext.Provider value={[state, dispatch]}>
      <Header></Header>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Checkout></Checkout>}></Route>
          <Route path="/denied" element={<Denied></Denied>}></Route>
        </Routes>
      </BrowserRouter>
    </CheckoutContext.Provider>
  );
};
