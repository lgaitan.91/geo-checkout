import { TextField } from "@mui/material";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import styled from "styled-components";

const theme = {
  primary: {
    background: "#8F2DF5",
    text: "#fff",
    color: "#fff",
  },
};

export const Box = styled.div`
  &.s-margin {
    margin: 20px;
  }
  &.s-margin-top {
    margin-top: 20px;
  }
  &.s-margin-right {
    margin-right: 20px;
  }
  &.s-margin-left {
    margin-left: 20px;
  }
  &.s-margin-bottom {
    margin-bottom: 20px;
  }
`;

export const FlexBox = styled.div`
  display: flex;
  width: 100%;
  align-items: ${(props) => props.alignItems || "center"};
  flex-direction: ${(props) => props.direction || "row"};
  justify-content: ${(props) => props.justifyContent || "center"};
  gap: ${(props) => props.gap || 0}px;
`;

export const Title = styled.h1`
  font-weight: 500;
  font-size: 22px;
  line-height: 24px;
  text-align: ${(props) => props.textAlign || "left"};
`;

export const Button = styled.button`
  background: ${theme.primary.background};
  color: ${theme.primary.color};
  border-width: 0;
  border-radius: 24px;
  width: 100%;
  height: 44px;
`;

export const Text = styled.label`
  font-size: 14px;
  font-weight: 500;

  &.muted {
    color: #585858;
  }

  &.contrast {
    color: #fff;
  }
`;

export const HighlightedText = styled.label`
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 22px;
  color: #8f2df5;
`;

export const Link = styled.a`
  color: #8f2df5;
`;

export const RadioButton = styled.input`
  border: 2px solid #b3b3b3;
  width: 24px;
  height: 24px;
`;

export const InputText = styled(TextField)`
  label.Mui-focused {
    color: ${theme.primary.background} !important;
  }

  div:after {
    border-bottom: 2px solid ${theme.primary.background};
  }

  input:invalid {
    border-bottom: 2px solid red;
  }
`;

export const ErrorIcon = styled(ErrorOutlineIcon)`
  font-size: 100px !important;
  color: #d63e49;
  width: 5rem;
  height: 5rem;
`;
