import React from "react";
import { Title, FlexBox, Text, Box, Link } from "../styles/styles";
import HttpsIcon from "@mui/icons-material/Https";
import { CheckoutForm } from "../components/CheckoutForm";
import { CartDetail } from "../components/CartDetail";

export const Checkout = () => {
  return (
    <Box className="s-margin">
      <Title textAlign="center">Completá los datos para pagar</Title>
      <CartDetail />
      <Box className="s-margin-top s-margin-bottom">
        <Text>
          Paga con tarjeta de crédito o débito. <Link>Ver tarjetas</Link>
        </Text>
      </Box>
      <CheckoutForm />
      <Box className="s-margin-top">
        <FlexBox>
          <HttpsIcon />
          <Text className="muted">Todas las transacciones son seguras</Text>
        </FlexBox>
      </Box>
    </Box>
  );
};
