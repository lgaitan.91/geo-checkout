import React from "react";
import { FlexBox, Title, Text, Button, Box, ErrorIcon } from "../styles/styles";
import { useNavigate } from "react-router-dom";

export const Denied = () => {
  const navigate = useNavigate();

  const onBackClick = () => {
    navigate(-1);
  };

  return (
    <>
      <Box className="s-margin">
        <FlexBox direction="column" gap={20}>
          <ErrorIcon />
          <Title>Transacción denegada</Title>
          <Text>
            ¡Disculpas! Se ha producido un error, por favor vuelve a intentar.
          </Text>
          <Button onClick={onBackClick}>Volver a intentar</Button>
        </FlexBox>
      </Box>
    </>
  );
};
