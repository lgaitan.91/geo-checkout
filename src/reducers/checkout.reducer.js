export const initialState = {
  data: {
    shop_name: "John's Shop",
    attributes: {
      total_price: 1000,
      total_price_accessibility: "1000 pesos",
      currency: "ARS",
      currency_symbol: "$",
      items: [
        {
          name: "Pizza Pequeña",
          quantity: 2,
          unitPrice: {
            amount: 250,
          },
        },
        {
          name: "Ensalada de Frutas",
          quantity: 1,
          unitPrice: {
            amount: 500,
          },
        },
      ],
      installments: [
        {
          installment: 1,
          total: 1000,
          financialRate: 0,
          installmentPrice: 1000,
          selected: true,
        },
        {
          installment: 3,
          total: 1122.21,
          financialRate: 0.09,
          installmentPrice: 374.07,
          selected: false,
        },
        {
          installment: 6,
          total: 1249.45,
          financialRate: 0.165,
          installmentPrice: 208.24,
          selected: false,
        },
      ],
    },
  },
};

export const CheckoutReducer = (state = initialState, action) => {
  switch (action.type) {
    case "installment-change": {
      let installment = state.data.attributes.installments.find(
        (i) => i.installment === action.payload
      );

      return {
        data: {
          ...state.data,
          attributes: {
            ...state.data.attributes,
            total_price: installment.total,
            installments: state.data.attributes.installments.map((i) => ({
              ...i,
              selected: i.installment === installment.installment,
            })),
          },
        },
      };
    }
    default:
      return state;
  }
};
