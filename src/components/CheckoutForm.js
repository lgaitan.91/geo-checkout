import React from "react";
import { CardForm } from "./CardForm";
import { InstallmentContainer } from "./InstallmentContainer";
import { PersonalData } from "./PersonalData";
import { Button } from "../styles/styles";
import { useNavigate } from "react-router-dom";
import useForm from "../hooks/useForm";

export const CheckoutForm = () => {
  const { formState, handleInputChange } = useForm({});
  const navigate = useNavigate();

  const onCheckout = (e) => {
    console.log(formState);
    e.preventDefault();
    navigate("/denied");
  };

  return (
    <form onSubmit={onCheckout}>
      <CardForm handleInputChange={handleInputChange} />
      <InstallmentContainer handleInputChange={handleInputChange} />
      <PersonalData handleInputChange={handleInputChange} />
      <Button type="submit">Continuar</Button>
    </form>
  );
};
