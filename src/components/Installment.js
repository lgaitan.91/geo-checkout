import React from "react";
import styled from "styled-components";
import { FlexBox, Text, RadioButton } from "../styles/styles";

const InstallmentOption = styled(FlexBox)`
  background-color: rgba(143, 45, 245, 0.07);
  padding: 10px;
  border-radius: 8px;
`;

export const Installment = ({
  installment,
  financialRate,
  installmentPrice,
  total,
  handleInputChange,
  installmentChange,
  selected,
}) => {
  const onInstallmentChange = (e) => {
    handleInputChange(e);
    installmentChange({
      type: "installment-change",
      payload: installment,
    });
  };

  return (
    <InstallmentOption>
      <RadioButton
        type="radio"
        name="installment"
        onChange={onInstallmentChange}
        value={installment}
        checked={selected}
      />
      <FlexBox justifyContent="space-between">
        <FlexBox direction="column" alignItems="left">
          <Text>
            {installment} cuotas de $ {installmentPrice}
          </Text>
          <Text className="muted">CF: {financialRate}%</Text>
        </FlexBox>
        <Text>{financialRate ? "$" + total : "Sin interés"}</Text>
      </FlexBox>
    </InstallmentOption>
  );
};
