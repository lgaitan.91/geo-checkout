import React, { useContext } from "react";
import { Installment } from "./Installment";
import { Title, FlexBox } from "../styles/styles";
import { CheckoutContext } from "../context/checkout.context";

export const InstallmentContainer = ({ handleInputChange }) => {
  const [
    {
      data: {
        attributes: { installments, currency_symbol: currencySymbol },
      },
    },
    dispatch,
  ] = useContext(CheckoutContext);

  return (
    <>
      <Title>Cuotas</Title>
      <FlexBox direction="column" gap={10}>
        {installments.map((installment) => {
          return (
            <Installment
              key={installment.installment}
              selected={installment.selected}
              handleInputChange={handleInputChange}
              installmentChange={dispatch}
              currencySymbol={currencySymbol}
              {...installment}
            />
          );
        })}
      </FlexBox>
    </>
  );
};
