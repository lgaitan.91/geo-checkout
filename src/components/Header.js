import React, { useContext } from "react";
import logo from "../assets/logo.svg";
import { CheckoutContext } from "../context/checkout.context";
import { Text } from "../styles/styles";
import styled from "styled-components";

const StyledHeader = styled.div`
  height: 49px;
  display: flex;
  align-items: center;
  padding: 10px;
  justify-content: space-between;
  background-color: #191c3c;
`;

const StyledImage = styled.img`
  width: 114px;
  height: 20px;
`;

export const Header = () => {
  const [
    {
      data: { shop_name: shopName },
    },
  ] = useContext(CheckoutContext);

  return (
    <StyledHeader>
      <StyledImage src={logo} alt="logo"></StyledImage>
      <Text className="contrast">{shopName}</Text>
    </StyledHeader>
  );
};
