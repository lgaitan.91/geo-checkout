import React from "react";
import { FlexBox, InputText } from "../styles/styles";
import HelpOutlineRoundedIcon from "@mui/icons-material/HelpOutlineRounded";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import { InputAdornment } from "@mui/material";

export const CardForm = ({ handleInputChange }) => {
  return (
    <>
      <InputText
        id="card-number"
        helperText="Número de tarjeta"
        label="Número de tarjeta"
        type="text"
        variant="standard"
        autoComplete="off"
        name="cardNumber"
        onChange={handleInputChange}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <CreditCardIcon />
            </InputAdornment>
          ),
        }}
        fullWidth
      />
      <FlexBox gap={10}>
        <InputText
          id="expiration-date"
          helperText="Fecha de expiración"
          onChange={handleInputChange}
          type="text"
          name="expirationDate"
          label="MM/AA"
          variant="standard"
          autoComplete="off"
        />
        <InputText
          helperText="3 números al dorso de tarjeta"
          id="credit card"
          label="Cód. de seguridad"
          type="text"
          variant="standard"
          name="code"
          autoComplete="off"
          onChange={handleInputChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <HelpOutlineRoundedIcon />
              </InputAdornment>
            ),
          }}
        ></InputText>
      </FlexBox>
      <InputText
        id="name"
        helperText="Como figura en la tarjeta"
        type="text"
        label="Nombre de titular"
        variant="standard"
        name="cardHolder"
        autoComplete="off"
        onChange={handleInputChange}
        fullWidth
      />
      <InputText
        id="document"
        helperText="Número de documento"
        type="text"
        name="document"
        label="DNI del titular"
        variant="standard"
        autoComplete="off"
        onChange={handleInputChange}
        fullWidth
      />
    </>
  );
};
