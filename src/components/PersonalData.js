import React from "react";
import { InputText, Title, Box } from "../styles/styles";

export const PersonalData = ({ handleInputChange }) => {
  return (
    <Box className="s-margin-bottom">
      <Title>Datos Personales</Title>
      <InputText
        id="email"
        helperText="A éste mail te enviaremos el resumen de compra"
        label="E-mail"
        onChange={handleInputChange}
        name="mail"
        type="text"
        variant="standard"
        autoComplete="false"
        inputProps={{
          pattern: "[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$",
        }}
        fullWidth
      />
    </Box>
  );
};
