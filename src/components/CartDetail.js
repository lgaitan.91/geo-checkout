import React, { useContext } from "react";
import styled from "styled-components";
import { CheckoutContext } from "../context/checkout.context";
import { FlexBox, Text, HighlightedText } from "../styles/styles";

const Box = styled.div`
  box-sizing: border-box;
  border: 1px solid #d8d8d8;
  border-radius: 8px;
  padding: 10px;
`;

export const CartDetail = () => {
  const [
    {
      data: {
        attributes: {
          total_price: totalPrice,
          currency_symbol: currencySymbol,
          items,
        },
      },
    },
  ] = useContext(CheckoutContext);

  return (
    <Box>
      <FlexBox justifyContent="space-between">
        <HighlightedText>Total</HighlightedText>
        <HighlightedText>
          {currencySymbol} {totalPrice}
        </HighlightedText>
      </FlexBox>
      {items.map((item) => {
        return (
          <FlexBox key={item.name} justifyContent="space-between">
            <Text className="muted">
              {item.name} x {item.quantity}
            </Text>
            <Text className="muted">
              {currencySymbol} {item.unitPrice.amount * item.quantity}
            </Text>
          </FlexBox>
        );
      })}
    </Box>
  );
};
